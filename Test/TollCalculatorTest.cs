﻿using System;
using Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class TollCalculatorTest
    {
        private readonly TollCalculator tollCalculator;

        public TollCalculatorTest()
        {
            this.tollCalculator = new TollCalculator();
        }

        [TestMethod]
        public void Should_ReturnFee_For_SpecificDate()
        {
            var car = new Car();
            var date = new DateTime(2019, 3, 1, 6, 0, 0);
            var tollFee = this.tollCalculator.GetTollFee(date, car);
            Assert.AreEqual<double>(8, tollFee);
        }

        [TestMethod]
        public void Should_ReturnZeroFee_When_TollFreeVehicle()
        {
            var motorbike = new Motorbike();
            var date = new DateTime(2019, 3, 1, 6, 0, 0);
            var tollFee = this.tollCalculator.GetTollFee(date, motorbike);
            Assert.AreEqual<double>(0, tollFee);
        }

        [TestMethod]
        public void Should_ReturnHighestFee_For_CrossingsInPastHour()
        {
            var car = new Car();
            var dateOne = new DateTime(2019, 3, 1, 6, 30, 0);
            var dateTwo = new DateTime(2019, 3, 1, 7, 0, 0);
            car.Crossings.Add(new Crossing(dateOne, 12));
            car.Crossings.Add(new Crossing(dateTwo, 18));

            var tollFee = this.tollCalculator.GetTollFee(dateTwo.AddMinutes(-60), car);
            Assert.AreEqual<double>(18, tollFee);
        }

        [TestMethod]
        public void Should_ReturnTotalFee_For_SpecificDate()
        {
            var car = new Car();
            car.Crossings.Add(new Crossing(new DateTime(2019, 10, 7, 7, 0, 0), 18));
            car.Crossings.Add(new Crossing(new DateTime(2019, 10, 7, 7, 29, 0), 18));
            car.Crossings.Add(new Crossing(new DateTime(2019, 10, 7, 7, 58, 0), 18));
            car.Crossings.Add(new Crossing(new DateTime(2019, 10, 4, 7, 0, 0), 18));

            var totalFee = tollCalculator.GetTotalTollFee(new DateTime(2019, 10, 7), car);
            Assert.AreEqual<double>(54, totalFee);
        }

        [TestMethod]
        public void Should_ReturnMaximumFee_When_TotalFeeIsOverMaximumFee()
        {
            var car = new Car();
            car.Crossings.Add(new Crossing(new DateTime(2019, 10, 7, 7, 0, 0), 18));
            car.Crossings.Add(new Crossing(new DateTime(2019, 10, 7, 7, 29, 0), 18));
            car.Crossings.Add(new Crossing(new DateTime(2019, 10, 7, 7, 58, 0), 18));
            car.Crossings.Add(new Crossing(new DateTime(2019, 10, 7, 7, 15, 0), 18));

            var totalFee = tollCalculator.GetTotalTollFee(new DateTime(2019, 10, 7), car);
            Assert.AreEqual<double>(60, totalFee);
        } 
    }
}
