﻿using System;
using System.Collections.Generic;

namespace Calculator
{
    public abstract class Vehicle
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// The crossings.
        /// </summary>
        public IList<Crossing> Crossings { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vehicle"/> class.
        /// </summary>
        public Vehicle()
        {
            this.Id = Guid.NewGuid();
            this.Crossings = new List<Crossing>();
        }

        /// <summary>
        /// Determines whether the <see cref="Vehicle"/> is a toll free vehicle.
        /// </summary>
        public abstract bool IsTollFree();
    }

    public class Car : Vehicle
    {
        /// <inheritdoc/>
        public override bool IsTollFree()
        {
            return false;
        }
    }

    public class Motorbike : Vehicle
    {
        /// <inheritdoc/>
        public override bool IsTollFree()
        {
            return true;
        }
    }
}
