﻿using System;

namespace Calculator
{
    /**
     * 
     * This class represent the interval which will be added and used in the <see cref="TollFeeSchedule"/>.
     * 
     */
    public class TollFeeTime
    {
        /// <summary>
        /// The start time.
        /// </summary>
        public TimeSpan StartTime { get; private set; }

        /// <summary>
        /// The end time.
        /// </summary>
        public TimeSpan EndTime { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TollFeeTime"/> class.
        /// </summary>
        public TollFeeTime(TimeSpan startTime, TimeSpan endTime)
        {
            StartTime = startTime;
            EndTime = endTime;
        }

        /// <summary>
        /// Determines whether given time is within the interval of start to end time.
        /// </summary>
        public bool IsWithinInterval(TimeSpan timeSpan)
        {
            return timeSpan >= StartTime && timeSpan <= EndTime;
        }
    }
}
