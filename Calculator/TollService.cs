﻿using System;

namespace Calculator
{
    public interface ITollService
    {
        /// <summary>
        /// Get the total toll fee for a given date.
        /// </summary>
        double GetTotalFee(Guid vehicleId, DateTime date);

        /// <summary>
        /// Create a new crossing.
        /// </summary>
        void CreateCrossing(Guid vehicleId);
    }

    /**
     * 
     * This service layer will handle all the operations regarding the toll fee and will also be the layer between
     * the business and the presentation layer (if any).
     * 
     */
    public class TollService : ITollService
    {
        /// <summary>
        /// The <see cref="IVehicleService"/>
        /// </summary>
        private readonly IVehicleService vehicleService;

        /// <summary>
        /// The <see cref="ITollCalculator"/>
        /// </summary>
        private readonly ITollCalculator tollCalculator;

        /// <summary>
        /// Initializes a new instance of the <see cref="TollService"/> class.
        /// </summary>
        public TollService(IVehicleService vehicleService, ITollCalculator tollCalculator)
        {
            this.vehicleService = vehicleService;
            this.tollCalculator = tollCalculator;
        }

        /// <inheritdoc/>
        public void CreateCrossing(Guid vehicleId)
        {
            var vehicle = this.vehicleService.Get(vehicleId);
            var fee = this.tollCalculator.GetTollFee(vehicle);
            vehicle.Crossings.Add(new Crossing(fee));
        }

        /// <inheritdoc/>
        public double GetTotalFee(Guid vehicleId, DateTime date)
        {
            var vehicle = this.vehicleService.Get(vehicleId);
            return this.tollCalculator.GetTotalTollFee(date, vehicle);
        }
    }
}
