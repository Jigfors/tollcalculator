﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    /**
     * 
     * Handle all the different toll fees depending on time of day.
     * What's left:
     * - There should be some logic to handle all the different timezones and formats etc.
     * 
     */
    public class TollFeeSchedule
    {
        /// <summary>
        /// Contains all toll fee depending on time of day. 
        /// </summary>
        private readonly IDictionary<TollFeeTime, double> schedule;

        /// <summary>
        /// Initializes a new instance of the <see cref="TollFeeSchedule"/> class.
        /// </summary>
        public TollFeeSchedule()
        {
            schedule = new Dictionary<TollFeeTime, double>();
            Init();
        }

        /// <summary>
        /// Get fee for given time.
        /// </summary>
        public double GetFee(TimeSpan timeSpan)
        {
            return schedule.FirstOrDefault(x => x.Key.IsWithinInterval(timeSpan)).Value;
        }

        /// <summary>
        /// Initialize all fees.
        /// </summary>
        private void Init()
        {
            schedule.Add(new TollFeeTime(new TimeSpan(6, 0, 0), new TimeSpan(6, 29, 0)), 8);
            schedule.Add(new TollFeeTime(new TimeSpan(6, 30, 0), new TimeSpan(6, 59, 0)), 12);
            schedule.Add(new TollFeeTime(new TimeSpan(7, 0, 0), new TimeSpan(7, 59, 0)), 18);
            schedule.Add(new TollFeeTime(new TimeSpan(8, 0, 0), new TimeSpan(8, 29, 0)), 12);
            schedule.Add(new TollFeeTime(new TimeSpan(8, 30, 0), new TimeSpan(14, 59, 0)), 8);
            schedule.Add(new TollFeeTime(new TimeSpan(15, 0, 0), new TimeSpan(15, 29, 0)), 12);
            schedule.Add(new TollFeeTime(new TimeSpan(15, 30, 0), new TimeSpan(16, 59, 0)), 18);
            schedule.Add(new TollFeeTime(new TimeSpan(17, 0, 0), new TimeSpan(17, 59, 0)), 12);
            schedule.Add(new TollFeeTime(new TimeSpan(18, 0, 0), new TimeSpan(18, 29, 0)), 8);
            schedule.Add(new TollFeeTime(new TimeSpan(18, 30, 0), new TimeSpan(5, 59, 0)), 0);
        }
    }
}
