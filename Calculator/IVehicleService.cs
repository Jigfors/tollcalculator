﻿using System;

namespace Calculator
{
    public interface IVehicleService
    {
        /// <summary>
        /// Get a vehicle by given id.
        /// </summary>
        Vehicle Get(Guid vehicleId);
    }
}
