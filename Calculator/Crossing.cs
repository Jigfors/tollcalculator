﻿using System;

namespace Calculator
{
    public class Crossing
    {
        /// <summary>
        /// The crossing date.
        /// </summary>
        public DateTime CrossingDate { get; private set; }

        /// <summary>
        /// The fee.
        /// </summary>
        public double Fee { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Crossing"/> class.
        /// </summary>
        public Crossing(double fee)
            : this(DateTime.Now, fee)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Crossing"/> class.
        /// </summary>
        public Crossing(DateTime date, double fee)
        {
            CrossingDate = date;
            Fee = fee;
        }
    }
}
