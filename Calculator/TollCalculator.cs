﻿using System;
using System.Linq;

namespace Calculator
{
    public interface ITollCalculator
    {
        /// <summary>
        /// Get toll fee for given <see cref="Vehicle"/> on date now.
        /// </summary>
        double GetTollFee(Vehicle vehicle);

        /// <summary>
        /// Get total toll fee for given <see cref="Vehicle"/> on the given date.
        /// </summary>
        double GetTotalTollFee(DateTime date, Vehicle vehicle);
    }

    /**
     * 
     * The class are able to calculate:
     * - total fee for a <see cref="Vehicle"/> and a specific date.
     * - the fee for a specific date.
     * 
     */
    public class TollCalculator : ITollCalculator
    {
        /// <summary>
        /// Max fee per day
        /// </summary>
        private const double MaxTollFeePerDay = 60;

        /// <summary>
        /// The <see cref="TollFeeSchedule"/>
        /// </summary>
        private readonly TollFeeSchedule schedule;

        /// <summary>
        /// Initializes a new instance of the <see cref="TollCalculator"/> class.
        /// </summary>
        public TollCalculator()
        {
            schedule = new TollFeeSchedule();
        }

        /// <summary>
        /// Get toll fee for given <see cref="Vehicle"/> on a specific date.
        /// </summary>
        public double GetTollFee(DateTime date, Vehicle vehicle)
        {
            if (IsTollFreeDate(date) || vehicle.IsTollFree())
            {
                return 0;
            }

            // Check for crossings in the past hour.
            var crossings = vehicle.Crossings
                .Where(x => x.CrossingDate >= date.AddHours(-1))
                .Select(x => x.Fee).ToList();

            crossings.Add(schedule.GetFee(date.TimeOfDay));

            return crossings
                .OrderBy(x => x)
                .Last();
        }

        /// <inheritdoc/>
        public double GetTollFee(Vehicle vehicle)
        {
            var now = DateTime.Now;
            return GetTollFee(DateTime.Now, vehicle);
        }

        /// <inheritdoc/>
        public double GetTotalTollFee(DateTime date, Vehicle vehicle)
        {
            var totalFee = vehicle.Crossings
                   .Where(d => d.CrossingDate.Date == date)
                   .Sum(x => x.Fee);

            return totalFee > MaxTollFeePerDay ? MaxTollFeePerDay : totalFee;
        }

        /// <summary>
        /// Determines whether the given date is toll free.
        /// </summary>
        private bool IsTollFreeDate(DateTime date)
        {
            int year = date.Year;
            int month = date.Month;
            int day = date.Day;

            if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday) return true;

            if (year == 2013)
            {
                if (month == 1 && day == 1 ||
                    month == 3 && (day == 28 || day == 29) ||
                    month == 4 && (day == 1 || day == 30) ||
                    month == 5 && (day == 1 || day == 8 || day == 9) ||
                    month == 6 && (day == 5 || day == 6 || day == 21) ||
                    month == 7 ||
                    month == 11 && day == 1 ||
                    month == 12 && (day == 24 || day == 25 || day == 26 || day == 31))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
